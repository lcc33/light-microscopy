from matplotlib import pyplot as plt
import os
import sys
from tkinter import *
import json
import pandas as pd


sys.path.append('lib')

from lib import scatter_builder as sb
from file_handler import *
from cell_handler import *
from lib import multi_file_handler as mfh

magnification = 100

def get_connected_files(starting_directory, channel_names):
  root = Tk()
  result = mfh.MultiFileHandler(starting_directory, root, channel_names)
  root.mainloop()
  connected_files = result.get_channels()
  root.destroy()
  return connected_files

def get_cell_centers(main_file_name):
  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.set_title('click approximate cell centers')
  scatter = ax.scatter(0, 0, c='red', s=2)  
  image = open_image(main_file_name)
  ax.imshow(image)
  points = sb.ScatterBuilder(scatter)
  plt.show()
  points = points.get_points()
  return points

def get_anchor_points(connected_files):
  files = list(connected_files.values())
  fig = plt.figure()
  ax1 = fig.add_subplot(121)
  ax1.set_title('click anchor points')
  scatter1 = ax1.scatter(0, 0, c='red', s=2)  
  image = open_image(files[0])
  ax1.imshow(image)
  points1 = sb.ScatterBuilder(scatter1)

  ax2 = fig.add_subplot(122)
  ax2.set_title('click anchor points')
  scatter2 = ax2.scatter(0, 0, c='red', s=2)  
  image = open_image(files[1])
  ax2.imshow(image)
  points2 = sb.ScatterBuilder(scatter2)

  plt.show()
  points1 = points1.get_points()
  points2 = points2.get_points()
  offset = points2 - points1
  return offset

def extract_cells(points, channel_names, connected_files, magnification, anchor_points):
  output_data = pd.DataFrame()
  channel_names = list(connected_files.keys())[0:-1]
  for cell_number in range(len(points)):
    point = points[cell_number,:]
    output_data_row = {}
    key_save = []
    channel_index = 0
    for channel_name in channel_names:
      file_name = connected_files[channel_name]
      if file_name:
        if channel_index==0:
          image_offset = [0, 0]
        else:
          image_offset = anchor_points[0]
        result = CellHandler(file_name, point, cell_number, magnification, key_save, image_offset)
        if len(key_save)<1:
          key_save = result.get_key_save()
        output_data_row[channel_name] = get_replicate_name(file_name) + '_cell_' + str(cell_number) + '.csv'
      channel_index += 1
    output_data = output_data.append(output_data_row, ignore_index=True)
  return output_data

def export_single_cell_connections(main_file_name, output_data):
  sample_name = get_sample_name(main_file_name)
  output_file_name = 'single_cell_images/' + sample_name + '/cell.log'
  if os.path.exists(output_file_name):
    output_data.to_csv(output_file_name, mode='a', index=False, header=False)
  else:
    output_data.to_csv(output_file_name, mode='w', index=False, header=True)

starting_directory = '/home/lauren/Documents/research/light-microscopy/raw_images/3-30-21/ts_30deg_0min'

while True:
  channel_names = ['fm4-64', 'bf']
  #connected_files = get_connected_files(starting_directory, channel_names)
  connected_files = {'bf': '/home/lauren/Documents/research/light-microscopy/raw_images/3-30-21/ts_30deg_0min/SNAP-161311-0002.tif', 'fm4-64': '/home/lauren/Documents/research/light-microscopy/raw_images/3-30-21/ts_30deg_0min/SNAP-161259-0001.tif', 'main': 'fm4-64'}
  #connected_files = {'fm4-64': '/home/lauren/Documents/research/light-microscopy/raw_images/3-30-21/ts_30deg_0min/SNAP-161259-0001.tif', 'bf': '/home/lauren/Documents/research/light-microscopy/raw_images/3-30-21/ts_30deg_0min/SNAP-161311-0002.tif', 'main': 'fm4-64'}
  # First connected_files dictionary works with anchor points, but second one does.
  print(connected_files)
  main_file_name = connected_files[connected_files['main']]
  points = get_cell_centers(main_file_name)
  anchor_points = get_anchor_points(connected_files)
  output_data = extract_cells(points, channel_names, connected_files, magnification, anchor_points)
  export_single_cell_connections(main_file_name, output_data)
  starting_directory = os.path.dirname(main_file_name)



