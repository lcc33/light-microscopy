import numpy as np
import matplotlib.pyplot as plt

from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import seaborn as sns
import os
import csv
import sys
import seaborn 

sys.path.append('lib')


from lib import sample_handler as sh
from lib import cell_comparison as cc
from lib import cell_stats




def extract_cell_log(path_name):
  with open(path_name+'/cell.log', newline='') as f:
    reader = csv.reader(f)
    cell_log = list(reader)
  channel_names = cell_log[0]
  cell_log = cell_log[1:]
  return channel_names, cell_log


def analyze_cells(path_name, cell_log, magnification, ring_threshold):
  plt.rc('figure', figsize=(8.5, 11))
  sample_name = os.path.split(path_name)[-1]
  pp = PdfPages('results/' + sample_name + '_images.pdf')
  center_intensity_difference = np.zeros((len(cell_log), len(cell_log[0])))
  ind = 0
  for file_names in cell_log:
    center_intensity_difference[ind, :] = analyze_cell(file_names, path_name, magnification, ring_threshold)
    pp.savefig()
    ind = ind + 1
  pp.close()
  return center_intensity_difference

def analyze_cell(file_names, path_name, magnification, ring_threshold):
  plt.clf()
  center_intensity_difference = np.zeros(len(file_names))
  for ii in range(len(file_names)):
    file_name = path_name + '/' + file_names[ii]
    if os.path.exists(file_name):
      single_cell = pd.read_csv(file_name).values
      cross_sections = cell_stats.extract_cross_sections(single_cell, magnification)
      center_intensity_difference[ii] = cell_stats.find_center_intensity_difference(cross_sections)
      cell_widths = cell_stats.find_cell_width(cross_sections)
      #focus = cell_stats.determine_focus(cell_widths, magnification)
      focus=True
      ring = cell_stats.determine_ring(center_intensity_difference[ii], ring_threshold)
      
      plt.subplot(3, 3, ii*3+1)
      plt.imshow(single_cell)
      plt.subplot(3, 3, ii*3+2)
      for jj in range(3):
        cross_section = cross_sections[jj,:]
        plt.plot(cross_section)
      plt.subplot(3, 3, ii*3+3)
      plt.text(0.1, 0.9, 'cell width: ' + str(cell_widths))
      plt.text(0.1, 0.8, 'focus: ' + str(focus))
      plt.text(0.1, 0.7, 'center int. diff.: ' + str(round(center_intensity_difference[ii], 2)))
      plt.text(0.1, 0.6, 'ring: ' + str(ring))
      plt.axis('off')
      center_intensity_difference[ii] = center_intensity_difference[ii] * focus
  return center_intensity_difference

def extract_parameters(path_name):
  ind_start = path_name.find('ts_') + 3
  ind_end = path_name.find('deg')
  temperature = float(path_name[ind_start:ind_end])
  ind_start = path_name.find('deg_') + 4
  ind_end = path_name.find('min')
  if not "min" in path_name:
    ind_end = path_name.find('sec')
    time = float(path_name[ind_start:ind_end])/60
  else: 
    time = float(path_name[ind_start:ind_end])
  return time, temperature

def analyze_channels(results_all, channel_names, ring_threshold):
  plt.rc('figure', figsize=(8.5, 11))
  pp = PdfPages('results/ring_plot.pdf')
  for channel_name in channel_names:
    if channel_name != 'bf':
      analyze_channel(results_all, channel_name, ring_threshold)
      pp.savefig()
  pp.close()

def analyze_channel(results_all, channel_name, ring_threshold):
  plt.clf()
  results_focused = results_all.loc[results_all[channel_name] > 0.1]

  seaborn.stripplot(x='time', y=channel_name, hue='temperature', data=results_focused)
  print('_________')
  print(channel_name)
  rings = results_focused[channel_name] > ring_threshold
  results_focused.insert(results_focused.shape[1], 'ring', rings.values)
  ring_count = results_focused.groupby(['temperature', 'time'])['ring'].sum()
  cell_count = results_focused.groupby(['temperature', 'time'])['ring'].count()
  final = pd.DataFrame(ring_count)
  final.insert(1, 'cell_count', cell_count)
  final.insert(2, 'ring_proportion', ring_count/cell_count)
  final.rename(columns={'ring': 'ring_count'}, inplace=True)
  print(final)
  file_name = 'results/' + channel_name + '_rings.csv'
  final.to_csv(file_name)

magnification = 100
folder_main = 'single_cell_images/2-5-21'
ring_threshold = 1.5

path_names = sh.extract_path_names(folder_main)
results_all = pd.DataFrame()
for path_name in path_names:
  #path_name = path_names[0]
  time, temperature = extract_parameters(path_name)
  channel_names, cell_log = extract_cell_log(path_name)
  center_intensity_difference = analyze_cells(path_name, cell_log, magnification, ring_threshold)
  results = pd.DataFrame(data=center_intensity_difference, columns=channel_names)
  results['temperature'] = temperature
  results['time'] = time
  results_all = results_all.append(results)
print(results_all)
analyze_channels(results_all, channel_names, ring_threshold)





