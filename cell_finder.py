from matplotlib import pyplot as plt
import numpy as np 
import os
import sys
import json

sys.path.append('lib')

from lib import scatter_builder as sb
from file_handler import *
from cell_handler import *

starting_directory = 'raw_images'
magnification = 100

while True: 
  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.set_title('click approximate cell centers')
  scatter = ax.scatter(0, 0, c='red', s=2)  
  file_name = new_file(ax, starting_directory)
  points = sb.ScatterBuilder(scatter)
  plt.show()

  points = points.get_points()

  for cell_number in range(len(points)):
    point = points[cell_number,:]
    CellHandler(file_name, point, cell_number, magnification)

  starting_directory = os.path.dirname(file_name)
