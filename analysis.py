import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from lib import sample_handler as sb
from lib import cell_comparison as cc

folder_main = 'single_cell_images/7-21-20'

samples = sb.SampleHandler(folder_main)

cell_comparison = cc.CellComparison(samples, 100)