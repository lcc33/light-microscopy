import numpy as np
import scipy.stats as stats
import math
import os
import pandas as pd
import matplotlib.pyplot as plt
from scipy import ndimage

def determine_angle(center, im):
  cell_ideal = np.zeros((100, 30))
  cell_ideal[25:75, 10:20] = 1
  cell_ideal[45:55, 10:20] = 10
  angle = np.linspace(0, 178, num=88)
  angle_fit = np.zeros(len(angle))
  
  if (im.shape[0]>30) and (im.shape[1]>30):
    for jj in range(len(angle)):
      cell_rot = ndimage.rotate(cell_ideal, angle[jj])
      height = int(cell_rot.shape[0]/2)
      width = int(cell_rot.shape[1]/2)
      if (height*2+2<im.shape[0]) and (width*2+2<im.shape[1]):
        cell_rot = cell_rot[0:height*2,0:width*2]
        cell_neighborhood = im[center[0]-height:center[0]+height, center[1]-width:center[1]+width]
        angle_fit[jj] = np.sum(np.multiply(cell_neighborhood, cell_rot))
  angle = 360 - angle[np.argmax(angle_fit)]
  return angle

def determine_width(single_cell, location):
  cut_depth = 3
  cross_section = single_cell[location-cut_depth:location+cut_depth+1,15:45]
  cross_section = np.sum(cross_section, axis=0)
  left, right = fwhm_boundaries(cross_section)
  width = right - left
  return width

def find_cell_center(single_cell):
  cross_section = np.sum(single_cell[45:55,10:-10], axis=0)
  left_ind, right_ind = fwhm_boundaries(cross_section)
  center_x = int(np.mean([left_ind, right_ind]))+10
  cross_section = np.sum(single_cell[10:-10, center_x-5:center_x+6], axis=1)
  bottom_ind, top_ind = fwhm_boundaries(cross_section)
  center_y = int(np.mean([bottom_ind, top_ind]))
  center_y += 10
  return center_x, center_y

def extract_cross_sections(single_cell, cross_section_locations):
  cross_sections = np.zeros((3, 40))
  cut_depth = 0
  for ii in range(3):
    cross_section = single_cell[cross_section_locations[ii]-cut_depth:cross_section_locations[ii]+cut_depth+1,10:50]
    cuts = cross_section.shape[0]
    cross_section = np.sum(cross_section, axis=0)
    cross_sections[ii, ] = np.divide(cross_section, cuts)
  return cross_sections

def extract_cross_section_neighborhoods(single_cell, cross_section_locations):
  cut_depth = 1
  cross_sections = np.zeros((3, 40*3))
  for ii in range(3):
    cross_section = single_cell[cross_section_locations[ii]-cut_depth:cross_section_locations[ii]+cut_depth+1,10:50]
    cross_sections[ii, ] = cross_section.flatten()
  return cross_sections

def determine_intensity_diff(single_cell, cross_section_locations):
  cross_sections = extract_cross_sections(single_cell, cross_section_locations)
  quarter_min_1, quarter_max_1 = determine_internal_min_max(cross_sections[0,])
  quarter_min_2, quarter_max_2 = determine_internal_min_max(cross_sections[2,])
  quarter_max = (quarter_max_1 + quarter_max_2)/2
  quarter_min = (quarter_min_1 + quarter_min_2)/2
  center_min, center_max = determine_internal_min_max(cross_sections[1,])
  center_intensity_difference = (center_max-center_min)/(quarter_max-quarter_min)
  return center_intensity_difference

def determine_internal_min_max(cross_section):
  section_sort = np.sort(cross_section)
  section_max = np.mean(section_sort[-5:-1])
  section_min = np.mean(section_sort[0:5])
  return section_min, section_max

def fwhm_boundaries(cross_section):
  cross_section = cross_section - np.min(cross_section)
  min_mean = np.mean([cross_section[0:5], cross_section[-6:-1]])
  max_mean = np.mean(np.sort(cross_section)[-4:-1])
  center_ind = int(round(np.mean(np.argsort(cross_section)[-4:-1])))
  mid = (max_mean-min_mean)/2
  distance_from_mid = np.abs(np.subtract(cross_section, mid))
  left_ind = np.argmin(distance_from_mid[0:center_ind])
  right_ind = np.argmin(distance_from_mid[center_ind:-1])+center_ind
  return left_ind, right_ind

def calculate_stats(results, samples, variable):
  same_as_unfixed = [False]*(len(samples)-1)
  for ii in range(1, len(samples)):
    sample_1 = results.loc[results['sample'] == samples[0]]
    data_1 = sample_1[variable]
    sample_2 = results.loc[results['sample'] == samples[ii]]
    data_2 = sample_2[variable]
    if welchs_ttest(data_1, data_2):
      same_as_unfixed[ii-1] = True
  return same_as_unfixed
      
def welchs_ttest(data_1, data_2):
  t = (data_1.mean() - data_2.mean())/math.sqrt(data_1.var()**2/len(data_1) + data_2.var()**2/len(data_2))
  num = data_1.var()**2/len(data_1) + data_2.var()**2/len(data_2)
  den = data_1.var()**4/(len(data_1)**2*(len(data_1)-1)) + data_2.var()**4/(len(data_2)**2*(len(data_2)-1))
  df = round(num**2/den)
  p = stats.t.pdf(t, df)
  if (p < 0.05):
    return True
  else:
    return False

def list_mean(list):
  return sum(list)/len(list)

def determine_focus(top_width, bottom_width, center_width):
  cell_count = len(top_width)
  focused = [False]*cell_count
  for ii in range(cell_count):
    width_diff = (top_width[ii]-bottom_width[ii])**2
    if (width_diff<25) & (top_width[ii]<15) & (bottom_width[ii]<15) & (center_width[ii]<15):
      if (top_width[ii]>5) & (bottom_width[ii]>5) & (center_width[ii]>5):
        focused[ii] = True
  return focused

def determine_width_ratio(top_width, bottom_width, center_width):
  cell_count = len(top_width)
  width_ratio = np.zeros(cell_count)
  for ii in range(cell_count):
    cell_width = (top_width[ii]+bottom_width[ii])/2
    width_ratio[ii] = center_width[ii]/cell_width
  return width_ratio

def import_cell(file_name):
  single_cell = np.genfromtxt(open(file_name, "rb"), delimiter=",", skip_header=0, dtype=int)
  return single_cell
      

def analyze_rings(path, intensity_cutoff):
  file_names = os.listdir(path)
  cells_intensity_diff = []
  center_width = []
  top_width = []
  bottom_width = []
  
  for ii in range(len(file_names)):
    file_name = os.path.join(path, file_names[ii])
    if (file_name.endswith('.csv')):
      single_cell = import_cell(file_name)
      center_x, center_y = find_cell_center(single_cell)
      center_y = 50
      cross_section_locations = [center_y-10, center_y, center_y+10]
      intensity_diff = determine_intensity_diff(single_cell, cross_section_locations)
      center = determine_width(single_cell, cross_section_locations[1])
      top = determine_width(single_cell, cross_section_locations[0])
      bottom = determine_width(single_cell, cross_section_locations[2])
      center_width.append(center)
      top_width.append(top)
      bottom_width.append(bottom)
      cells_intensity_diff.append(intensity_diff)
  focused = determine_focus(top_width, bottom_width, center_width)
  width_ratio = determine_width_ratio(top_width, bottom_width, center_width)
  ring = [False]*len(cells_intensity_diff)
  for ii in range(len(cells_intensity_diff)):
    if (cells_intensity_diff[ii]>intensity_cutoff):
      ring[ii] = True
  results = {
    'intensity_diff': cells_intensity_diff,
    'center_width': center_width,
    'top_width': top_width,
    'bottom_width': bottom_width,
    'width_ratio': width_ratio,
    'focused': focused,
    'ring_present': ring
  }
  return results

def composite_image(frame):
  image = np.sum(frame, axis=2)
  kernel = np.ones((5, 5))
  image_sum = ndimage.convolve(image, kernel)
 # image_sum = np.pad(image_sum, 50, 'minimum')
  return image_sum

def center_frame(frame):
  image = composite_image(frame)
  center = np.unravel_index(np.argmax(image, axis=None), image.shape)
  radius = 50
  frame = frame[center[0]-radius:center[0]+radius, center[1]-radius:center[1]+radius, :]
  return frame

def plot_cross_sections(single_cell):
  height = 50
  width = 30
  center_y = int(single_cell.shape[0]/2)
  center_x = int(single_cell.shape[1]/2)
  for jj in range(3):
    cross_section = single_cell[center_y+10*jj-10,center_x-width:center_x+width]
    plt.plot(cross_section)

