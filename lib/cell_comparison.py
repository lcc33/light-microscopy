import numpy as np
import matplotlib.pyplot as plt

from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import seaborn as sns
import os
import sys

sys.path.append('lib')

from cell_stats import *

from lib import single_cell as sc
from lib import sample_handler as sb
from lib import cell_comparison as cc


class CellComparison():
  def __init__(self, samples, magnification):
    self.samples = samples
    self.magnification = magnification
    self.extract_results()
    self.plot_example_cells()
    self.count_rings()
    self.compare_variable('ring_height')
    self.compare_variable('width_to_height')
    self.compare_variable('intensity_diff')
    self.plot_results()

  def extract_results(self):
    self.results = pd.DataFrame()
    for ii in range(self.samples.samples_count):
      self.samples.set_active_sample(ii)
      for jj in range(self.samples.files_count):
        file_name = self.samples.files[jj]
        single_cell = sc.SingleCell(file_name, self.magnification)
        self.results = self.results.append(single_cell.results, ignore_index = True)
    self.results_focused = self.results.loc[self.results['focused']==True]

  def plot_example_cells(self):
    plt.rc('figure', figsize=(8, 6))
    pp = PdfPages('results/cell_images.pdf')
    for ii in range(self.samples.samples_count):
      self.samples.set_active_sample(ii)
      cells_to_plot = np.min([self.samples.files_count, 60])
      for jj in range(cells_to_plot):
        file_name = self.samples.files[jj]
        single_cell = sc.SingleCell(file_name, self.magnification)
        plt.clf()
        single_cell.plot_summary()
        pp.savefig()
    pp.close()

  def compare_variable(self, variable):
    comparison = compare_variable(self.results_focused, variable)
    plt.rc('figure', figsize=(8.5, 11))
    file_name = 'compare_' + variable + '.pdf'
    file_name = os.path.join('results', file_name)
    pp = PdfPages(file_name)
    plt.clf()
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.4)
    sns.swarmplot(x="sample", y=variable, data=self.results, hue='focused')
    plt.xticks(rotation=90)
    plt.draw()
    pp.savefig()
    plt.clf()
    plt.text(0.1, 0.1, comparison.to_string())
    plt.draw()
    pp.savefig()
    pp.close()

  def count_rings(self):
    sample_names = self.results_focused['sample'].unique()
    results = pd.DataFrame()
    results['sample_names'] = sample_names
    results['total_cells'] = 0
    results['total_rings'] = 0
    for ii in range(len(sample_names)):
      sample_data = self.results_focused[self.results_focused['sample']==sample_names[ii]]
      total_cells = sample_data.shape[0]
      total_rings = sum(sample_data['intensity_diff'] > 1.4)
      results['total_cells'].loc[ii] = total_cells
      results['total_rings'].loc[ii] = total_rings
    results['ring_proportion'] = results['total_rings']/results['total_cells']
    print(results)

  def plot_results(self):
    plt.rc('figure', figsize=(11, 8.5))
    file_name = 'time_temperature_ring.pdf'
    file_name = os.path.join('results', file_name)
    pp = PdfPages(file_name)
    plt.clf()
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
   # self.results_focused.plot.scatter(x="time", y="intensity_diff",  c='temperature')
    self.results_30deg = self.results_focused.loc[self.results_focused['temperature']==30]
    self.results_30deg.boxplot(column="intensity_diff", by="time", ax=ax1)
    ax1.set_title('30 Degrees')
    ax1.set_ylabel('Center Intensity Difference')
    ax1.set_xlabel('Time (min)')
    self.results_42deg = self.results_focused.loc[self.results_focused['temperature']==42]
    self.results_42deg.boxplot(column="intensity_diff", by="time", ax=ax2)
    ax2.set_title('42 Degrees')
    ax2.set_xlabel('Time (min)')


    plt.draw()
    pp.savefig()
    pp.close()


    