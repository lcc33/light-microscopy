
  

def compare_variable(self, variable, channel_name):
  results_channel = self.results[self.results['channel_name'].str.contains(channel_name, na=False)]
  comparison = compare_variable(results_channel, variable)
  plt.rc('figure', figsize=(8.5, 11))
  file_name = 'compare_' + variable + '.pdf'
  file_name = os.path.join('results', file_name)
  pp = PdfPages(file_name)
  plt.clf()
  fig, ax = plt.subplots()
  fig.subplots_adjust(bottom=0.4)
  sns.swarmplot(x="sample", y=variable, data=results_channel, hue='focused')
  plt.xticks(rotation=90)
  plt.draw()
  pp.savefig()
  plt.clf()
  plt.text(0.1, 0.1, comparison.to_string())
  plt.draw()
  pp.savefig()
  pp.close()

def count_rings(self, channel_name):
  results_channel = self.results[self.results['channel_name'].str.contains(channel_name, na=False)]
  sample_names = results_channel['sample'].unique()
  results = pd.DataFrame()
  results['sample_names'] = sample_names
  results['total_cells'] = 0
  results['total_rings'] = 0
  for ii in range(len(sample_names)):
    sample_data = results_channel[results_channel['sample']==sample_names[ii]]
    total_cells = sample_data.shape[0]
    total_rings = sum(sample_data['intensity_diff'] > 1.4)
    results['total_cells'].loc[ii] = total_cells
    results['total_rings'].loc[ii] = total_rings
  results['ring_proportion'] = results['total_rings']/results['total_cells']
  print(results)

def plot_results(self):
  plt.rc('figure', figsize=(11, 8.5))
  file_name = 'time_temperature_ring.pdf'
  file_name = os.path.join('results', file_name)
  pp = PdfPages(file_name)
  channel_names = ['ftsn', 'ftsa']
  for channel_name in channel_names:
    plt.clf()
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
  # self.results_focused.plot.scatter(x="time", y="intensity_diff",  c='temperature')
    results_focused_channel = self.results_focused[self.results_focused['channel_name'].str.contains(channel_name, na=False)]
    results_30deg = results_focused_channel.loc[results_focused_channel['temperature']==30]
    print(results_30deg)
    results_30deg.boxplot(column="intensity_diff", by="time", ax=ax1)
    ax1.set_title('30 Degrees')
    ax1.set_ylabel('Center Intensity Difference')
    ax1.set_xlabel('Time (min)')
    results_42deg = results_focused_channel.loc[results_focused_channel['temperature']==42]
    results_42deg.boxplot(column="intensity_diff", by="time", ax=ax2)
    ax2.set_title('42 Degrees')
    ax2.set_xlabel('Time (min)')

    plt.draw()
    pp.savefig()
  pp.close()



#self.plot_example_cells()
#channel_names = ['ftsa', 'ftsn']
#for channel_name in channel_names:
#  self.count_rings(channel_name)
#  self.compare_variable('ring_height', channel_name)
#  self.compare_variable('width_to_height', channel_name)
#  self.compare_variable('intensity_diff', channel_name)
  #self.plot_results()

    