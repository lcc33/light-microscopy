import numpy as np
import math
import scipy.stats as stats
import pandas as pd


def extract_cross_sections(image, magnification):
  center = int(image.shape[0]/2)
  neighborhood_size = int(magnification/30)
  ind = [center-int(magnification/6), center, center+int(magnification/6)]

  cross_sections = np.zeros((3, image.shape[1]))
  for ii in range(3):
    cell_slice = image[ind[ii]-neighborhood_size:ind[ii]+neighborhood_size+1, :]
    cross_sections[ii, :] = np.mean(cell_slice, axis=0)
  return cross_sections

def determine_focus(cell_widths, magnification):
  if (np.mean(cell_widths)<magnification/4):
    focus = True
  else:
    focus = False
  return focus

def determine_ring(center_intensity_difference, threshold):
  if center_intensity_difference>threshold:
    ring = True
  else:
    ring = False
  return ring

def find_cell_width(cross_sections):
  cell_width = np.zeros(3)
  for ii in range(3):
    left, right = fwhm_boundaries(cross_sections[ii,:])
    cell_width[ii] = right - left
  return cell_width


def fwhm_boundaries(cross_section):
  cross_section = cross_section - np.min(cross_section)
  crop_ind = int(len(cross_section)/4)
  cross_section = cross_section[crop_ind:-crop_ind]
  wid = 10
  kernel = np.ones(wid)/wid
  #cross_section = np.convolve(cross_section, kernel, 'same')
  min_mean = np.mean([cross_section[0:5], cross_section[-6:-1]])
  max_mean = np.mean(np.sort(cross_section)[-4:-1])
  center_ind = int(round(np.mean(np.argsort(cross_section)[-4:-1])))
  mid = (max_mean-min_mean)/2
  distance_from_mid = np.abs(np.subtract(cross_section, mid))
  left_ind = np.argmin(distance_from_mid[0:center_ind])
  right_ind = np.argmin(distance_from_mid[center_ind:-1])+center_ind
  return left_ind+crop_ind, right_ind+crop_ind

def determine_internal_min_max(cross_section):
  section_sort = np.sort(cross_section)
  section_max = np.mean(section_sort[-5:-1])
  section_min = np.mean(section_sort[0:5])
  return section_min, section_max

def compare_variable(results, variable):
  samples = results['sample'].unique()
  same_as_unfixed = [False]*(len(samples))
  same_as_unfixed[0] = True
  sample_1 = results.loc[results['sample'] == samples[0]]
  data_1 = sample_1[variable]
  column_name = variable+'_p_value'
  comparison_result = pd.DataFrame()
  comparison_result['sample'] = samples
  p_value = np.zeros(len(samples))
  if len(sample_1)>1:
    for ii in range(1, len(samples)):
      comparison_result.at[ii, 'sample'] = samples[ii]
      sample_2 = results.loc[results['sample'] == samples[ii]]
      data_2 = sample_2[variable]
      p = welchs_ttest(data_1, data_2)
      p_value[ii] = p
  comparison_result[column_name] = p_value
  return comparison_result

def welchs_ttest(data_1, data_2):
  num = data_1.mean() - data_2.mean()
  den = data_1.var()**2/len(data_1) + data_2.var()**2/len(data_2)
  t = num/math.sqrt(den)
  num = data_1.var()**2/len(data_1) + data_2.var()**2/len(data_2)
  den = data_1.var()**4/(len(data_1)**2*(len(data_1)-1)) + data_2.var()**4/(len(data_2)**2*(len(data_2)-1))
  df = round(num**2/den)
  p = stats.t.pdf(t, df)
  return p



def find_center_intensity_difference(cross_sections):
  quarter_min_1, quarter_max_1 = determine_internal_min_max(cross_sections[0,])
  quarter_min_2, quarter_max_2 = determine_internal_min_max(cross_sections[2,])
  quarter_max = (quarter_max_1 + quarter_max_2)/2
  quarter_min = (quarter_min_1 + quarter_min_2)/2
  center_min, center_max = determine_internal_min_max(cross_sections[1,])
  center_intensity_difference = (center_max-center_min)/(quarter_max-quarter_min)
  return center_intensity_difference
