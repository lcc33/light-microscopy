import tkinter as tk
from tkinter import filedialog
from matplotlib import pyplot as plt
import os
import numpy as np 

def new_file(ax, starting_directory):
  file_name = get_file_name(starting_directory)
  #file_name = '/home/lauren/Documents/research/light-microscopy/7-2-20/FtsN-mNeon_pJSB2_JFL101/live_dil_430pm/SNAP-163629-0001.tif'
  if file_name:
    image = open_image(file_name)
    ax.imshow(image)
    sample_name = get_sample_name(file_name)
    make_output_directory(sample_name)
    return file_name
  else:
    quit()

def get_file_name(starting_directory):
  root = Tk()
  root.withdraw()
  file_name = filedialog.askopenfilename(initialdir = starting_directory, title = "Select file", filetypes = (("all files","*.*"),("tiff files","*.tif")))
  root.deiconify()
  root.destroy()
  return file_name

def open_image(file_name):
  image = plt.imread(file_name)
  image = np.pad(image, 100, 'mean')
  return image

def get_replicate_name(file_name):
  path = os.path.normpath(file_name)
  path = path.split(os.sep)
  replicate_name = path[-1]
  replicate_name = replicate_name[:-4]
  return replicate_name


def get_sample_name(file_name):
  path = os.path.normpath(file_name)
  path = path.split(os.sep)
  treatment_name = path[-2]
  index = -3
  while True:
    if (path[index] == 'raw_images'): 
      break
    else:
      treatment_name = os.path.join(path[index], treatment_name)
    index += -1
  return treatment_name

def make_output_directory(treatment_name):
  data_path = os.path.join('single_cell_images', treatment_name)
  if not os.path.exists(data_path):
    os.makedirs(data_path)
  