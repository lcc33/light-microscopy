import os


def extract_path_names(folder_main):
  path_names = []
  for folder in os.walk(folder_main):
    if any(folder.endswith('.log') for folder in os.listdir(folder[0])):
      path_names.append(folder[0])
  return path_names

def extract_file_names(sample_path):
  for root, dirs, file_names in os.walk(self.folder_main):
    for file_name in file_names:
      if file_name.endswith('.csv'):
        self.files.append(os.path.join(root, file_name))


def set_active_sample(self, active_sample):
  self.active_sample = active_sample
  self.extract_files()

  