from matplotlib import pyplot as plt
import numpy as np 

class ScatterBuilder:
  def __init__(self, scatter):
    self.scatter = scatter
    self.points = self.scatter.get_offsets()
    self.cid_1 = self.scatter.figure.canvas.mpl_connect('button_press_event', self.click_event)
    self.cid_2 = self.scatter.figure.canvas.mpl_connect('key_press_event', self.key_event)
    
  def click_event(self, event):
    if event.inaxes!=self.scatter.axes: return
    self.points = np.append(self.points, [[round(event.xdata), round(event.ydata)]], axis=0)
    self.scatter.set_offsets(self.points)
    self.scatter.figure.canvas.draw()

  def key_event(self, event):
    if event.key == 'q':
      quit()
    elif event.key != 'enter': return
    self.points = self.points[1:,]
    self.scatter.figure.canvas.mpl_disconnect(self.cid_1)
    self.scatter.figure.canvas.mpl_disconnect(self.cid_2)
    plt.close()
  
  def get_points(self):
    return self.points.astype(int)

