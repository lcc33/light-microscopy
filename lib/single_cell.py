import numpy as np
import matplotlib.pyplot as plt
import cell_stats
import pandas as pd
import os

class SingleCell():
  def __init__(self, file_name, magnification):
    self.file_name = file_name
    self.im = np.genfromtxt(open(file_name, "rb"), delimiter=",", skip_header=0, dtype=int)
    path = os.path.normpath(file_name)
    path = path.split(os.sep)
    self.magnification = magnification
    if (len(path)>2):
      self.sample_name = os.path.join(path[-3], path[-2])
    else:
      self.sample_name = path[-2]
    ind_start = self.sample_name.find('ts_') + 3
    ind_end = self.sample_name.find('deg')
    self.temperature = float(self.sample_name[ind_start:ind_end])
    ind_start = self.sample_name.find('deg_') + 4
    ind_end = self.sample_name.find('min')
    if not "min" in self.sample_name:
      ind_end = self.sample_name.find('sec')
      self.time = float(self.sample_name[ind_start:ind_end])/60
    else: 
      self.time = float(self.sample_name[ind_start:ind_end])
    
    path = os.path.dirname(file_name)
    cell_log = pd.read_csv(path + '/cell.log')
    fn = file_name.split(os.sep)[-1]
    result = cell_log.isin([fn]).any()  
    if len(result[result==True].index)>0:
      result = (result[result==True].index)[0]
    else:
      result = None
    self.channel_name = result
    self.quantify_cell()

  def plot_cross_sections(self, ax):
    colors = ['red', 'green', 'blue']
    labels = ['top', 'center', 'bottom']
    for ii in range(3):
      ax.plot(self.cell_cross_sections[ii,:], color=colors[ii], label=labels[ii])
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels)

  def generate_label(self):
    self.label = 'Cell Width: '
    for ii in range(3):
      self.label += str(self.width[ii]) + ' '
    self.label += '\nIntensity Diff: ' + str(round(self.center_intensity_difference,2))   
    self.label += ' Ring Height: ' + str(self.ring_height)
    self.label += ' Focus: ' + str(self.focus)
    
  def show_cell(self, ax):
    ax.imshow(self.im)
    ax.set_xlabel(self.label)

  def plot_summary(self):
    plt.suptitle(self.file_name, fontsize=12)
    ax = plt.subplot(2, 2, 2)
    self.plot_cross_sections(ax)
    ax = plt.subplot(2, 2, 4)
    self.plot_ring_cross_section(ax)
    ax = plt.subplot(1, 2, 1)
    self.show_cell(ax)
    plt.draw()

  def quantify_cell(self):
    self.extract_cross_sections(3)
    self.find_center_intensity_difference()
    self.find_cell_width()
    self.find_ring_height()
    self.find_width_to_height()
    self.determine_focus()
    self.generate_label()
    self.format_results_dictionary()

  def format_results_dictionary(self):
    self.results = {
      'sample': self.sample_name,
      'temperature': self.temperature,
      'time': self.time,
      'channel_name': self.channel_name,
      'intensity_diff': self.center_intensity_difference,
      'center_width': self.width[1],
      'top_width': self.width[0],
      'bottom_width': self.width[2],
      'ring_height': self.ring_height,
      'width_to_height': self.width_to_height,
      'focused': self.focus
    }

  def extract_cross_sections(self, neighborhood_size):
    center = int(self.im.shape[0]/2)
    ind = [center-int(self.magnification/6), center, center+int(self.magnification/6)]
    self.cell_cross_sections = np.zeros((3, self.im.shape[1]))
    self.ring_cross_sections = np.zeros((2, self.im.shape[0]-80))
    for ii in range(3):
      cell_slice = self.im[ind[ii]-neighborhood_size:ind[ii]+neighborhood_size+1, :]
      self.cell_cross_sections[ii, :] = np.mean(cell_slice, axis=0)
    cell_rotated = np.rot90(self.im)
    center = int(cell_rotated.shape[0]/2)
    self.ring_cross_sections[0,:] = np.mean(cell_rotated[center-5:center-3,40:-40], axis=0)
    self.ring_cross_sections[1,:] = np.mean(cell_rotated[center+3:center+5,40:-40], axis=0)


  def find_center_intensity_difference(self):
    quarter_min_1, quarter_max_1 = cell_stats.determine_internal_min_max(self.cell_cross_sections[0,])
    quarter_min_2, quarter_max_2 = cell_stats.determine_internal_min_max(self.cell_cross_sections[2,])
    quarter_max = (quarter_max_1 + quarter_max_2)/2
    quarter_min = (quarter_min_1 + quarter_min_2)/2
    center_min, center_max = cell_stats.determine_internal_min_max(self.cell_cross_sections[1,])
    self.center_intensity_difference = (center_max-center_min)/(quarter_max-quarter_min)

  def find_cell_width(self):
    self.width = np.zeros(3)
    for ii in range(3):
      left, right = cell_stats.fwhm_boundaries(self.cell_cross_sections[ii,:])
      self.width[ii] = right - left

  def find_ring_height(self):
    bright_ring = np.argmax((np.sum(self.ring_cross_sections[0,:]), np.sum(self.ring_cross_sections[1, :])))
    left, right = cell_stats.fwhm_boundaries(self.ring_cross_sections[bright_ring,:])
    self.ring_height = right-left

  def plot_ring_cross_section(self, ax):
    ax.plot(self.ring_cross_sections[0,:])
    ax.plot(self.ring_cross_sections[1,:])
    plt.legend(('Left', 'Right'))
  
  def find_width_to_height(self):
    self.width_to_height = self.width[1]/self.ring_height

  def determine_focus(self):
    if (self.width[1]<self.magnification/4):
      self.focus = True
    else:
      self.focus = False
  
