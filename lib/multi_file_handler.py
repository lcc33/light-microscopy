from tkinter import *
from tkinter import filedialog
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import os

class MultiFileHandler(Frame):
  def __init__(self, starting_folder, master, channel_names):
    Frame.__init__(self, master)
    self.master = master
    self.master.title("Image Processing")
    self.pack(fill=BOTH)
    self.channel_names = channel_names
    self.file_names = [None]*len(self.channel_names)
    self.main_channel = StringVar(self, channel_names[0])
    self.starting_folder = starting_folder
    for ii in range(len(self.channel_names)):
      self.create_channel_buttons(self.channel_names[ii], ii)
    self.create_controls()
    self.create_canvas()


  def create_controls(self):
    exit_button = Button(self, text="Exit", command=exit)
    exit_button.grid(row=3, column=5)
    export_button = Button(self, text="Export", command=self.export_channels)
    export_button.grid(row=3, column=6)

  def create_channel_buttons(self, channel_name, row):
    browse_button = Button(self)
    browse_button["text"] = channel_name
    browse_button["command"] = lambda: self.load_image(channel_name, row)
    browse_button.grid(row=row, column=0)
    Radiobutton(self, text=channel_name, variable=self.main_channel, value=channel_name).grid(row=row, column=3)

  def create_canvas(self):
    hf, ha = plt.subplots(2, 2)
    ha[-1, -1].axis('off')
    self.canvas = FigureCanvasTkAgg(hf, master=self.master)
    self.canvas.draw()
    self.canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)

  def load_image(self, channel_name, row):
    filename = filedialog.askopenfilename(initialdir=self.starting_folder, filetypes = (("image files", "*.tif") ,("All files", "*.*") ))
    self.starting_folder = os.path.dirname(filename)
    self.file_names[row] = filename
    image = plt.imread(filename)
    axs = plt.subplot(2, 2, row+1)
    plt.sca(axs)
    plt.imshow(image)
    plt.title(channel_name)
    plt.xlabel(filename[-40:-1])
    self.canvas.draw()
    self.canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)

  def export_channels(self):
    self.output_library = dict(zip(self.channel_names, self.file_names))
    self.output_library['main'] = self.main_channel.get()
    plt.close()
    
  def get_channels(self):
    return self.output_library.copy()
  




