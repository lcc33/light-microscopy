import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt
import ring_analysis_lib as lib
import read_lif
from scipy import ndimage
import os
from matplotlib.backends.backend_pdf import PdfPages
from single_cell import SingleCell
import csv


def straigten_frame(frame):
  frame = np.pad(frame, 20, 'mean')
  frame = lib.center_frame(frame)
  center = int(frame.shape[0]/2)
  image_sum = lib.composite_image(frame)
  angle = lib.determine_angle([center, center], image_sum)
  frame = ndimage.rotate(frame, angle, axes=(1, 0), reshape=False)
  return frame

def find_frame_snr(frame):
  frame_count = frame.shape[2]
  snr = np.zeros(frame_count)
  for ii in range(frame_count):
    image = frame[:, :, ii]
    center_y = int(image.shape[0]/2)
    center_x = int(image.shape[1]/2)
    center_values = image[center_y-5:center_y+6, center_x-5:center_x+6]
    center_sum = np.sum(center_values)
    total_sum = np.sum(image)
    snr[ii] = center_sum/total_sum
  return snr

def export_image(frame, snr, file_name, frame_number):
  focus_ind = np.argmax(snr)
  image = frame[:, :, focus_ind]
  image = image.astype(int)
  results_directory = 'confocal_images'
  if not os.path.isdir(results_directory):
    os.mkdir(results_directory)
  path = os.path.normpath(file_name)
  path = path.split(os.sep)
  output_file_name = path[-1][:-4] + '_' + str(frame_number) + '.csv'
  output_file_name = os.path.join(results_directory, output_file_name)
  print(output_file_name)
  np.savetxt(output_file_name, image, delimiter=',', fmt='%5.0u')
  return output_file_name

def plot_frame_summary(frame, single_cell, series_number):
  if (frame.shape[0]>5):
    plt.clf()
    plt.suptitle('Series: ' + str(series_number))
    image_ind = 1
    ax = plt.subplot(2, 3, image_ind)
    single_cell.plot_cross_sections(ax)
    focus_ind = np.argmax(snr)
    for ii in range(focus_ind-4, focus_ind+5, 2):
      image_ind += 1
      plt.subplot(2, 3, image_ind)
      image = frame[:, :, ii]
      plt.imshow(image)
      plt.title(ii)
      plt.draw()
    pp.savefig()

def pick_focus(file_name, series):
  plt.rc('figure', figsize=(10, 10))
  path = os.path.normpath(file_name)
  path = path.split(os.sep)
  output_file_name = 'results/confocal_images_' + path[-1][:-4] + '.pdf'
  pp = PdfPages(output_file_name)
  for jj in range(len(series)):
    frame = series[jj].getFrame()
    frame = straigten_frame(frame)
    if frame.shape[0]<1000 and frame.shape[1]<1000 and frame.shape[2]<100 and frame.shape[0]>40 and frame.shape[1]>40:
      snr = find_frame_snr(frame)
      output_file_name = export_image(frame, snr, file_name, jj)
      single_cell = SingleCell(output_file_name)
      plot_frame_summary(frame, single_cell, jj)
  pp.close()

frame_choice_file = '../6-17-19/frame_choice.csv'
frame_choice = pd.read_csv(frame_choice_file)
voxel_size_x = []
voxel_size_z = []

folder_name= '../6-17-19'
for ii in range(len(frame_choice)):
  file_name = frame_choice.iloc[ii]['treatment'] + '.lif'
  file_name = os.path.join(folder_name, 'images',  file_name)
  series_number = frame_choice.iloc[ii]['series']
  frame_number = frame_choice.iloc[ii]['frame']
  
  handler = read_lif.Reader(file_name).getSeries()[series_number-1]
  metadata = handler.getMetadata()
  voxel_size_x.append(metadata['voxel_size_x'])
  voxel_size_z.append(metadata['voxel_size_z'])

  image = handler.getFrame()[:, :, frame_number-1]
  output_file_name = os.path.join(folder_name, 'frames', frame_choice.iloc[ii]['treatment'] + '_series'+ str(series_number) + '_frame' + str(frame_number) + '.csv')
  np.savetxt(output_file_name, image, fmt='%f', delimiter=', ')

frame_choice['voxel_size_x'] = voxel_size_x
frame_choice['voxel_size_z'] = voxel_size_z

print(frame_choice)
frame_choice.to_csv(frame_choice_file, index=False)

  

