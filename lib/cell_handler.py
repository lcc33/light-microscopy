from matplotlib import pyplot as plt
import numpy as np 
from scipy import ndimage
import os

import file_handler as fh
import ring_analysis_lib as lib

class CellHandler:
  def __init__(self, file_name, center_point, cell_number, magnification, key_save, image_offset):
    plt.figure()
    self.file_name = file_name
    self.cell_number = cell_number
    self.magnification = magnification
    self.key_save = key_save
    self.magnitude = 1
    self.extract_cell(center_point, file_name, image_offset)
    self.init_figure()


  def update_figure(self):
    width = self.magnification
    height = self.magnification*2
    center_y = self.center_y
    center_x = self.center_x

    self.single_cell_axes.set_ylim([center_y-height, center_y+height])
    self.single_cell_axes.set_xlim([center_x-width, center_x+width])
    plt.sca(self.cross_section_axes)
    self.cross_section_axes.cla()
    single_cell_cropped = self.single_cell[center_y-height:center_y+height, center_x-width:center_x+width]
    lib.plot_cross_sections(single_cell_cropped)
    
    plt.draw()

  def extract_cell(self, center_point, file_name, image_offset):
    pad_size = self.magnification * 3
    image = fh.open_image(file_name)
    image = np.roll(image, -1*image_offset[0], axis=0)
    image = np.roll(image, -1*image_offset[1], axis=1)
    image = np.pad(image, pad_size, 'mean')

    height = pad_size
    width = pad_size
    center_point_pad = center_point + pad_size
    self.single_cell = image[center_point_pad[1]-height:center_point_pad[1]+height, center_point_pad[0]-width:center_point_pad[0]+width]

    self.center_x = int(self.single_cell.shape[1]/2)
    self.center_y = int(self.single_cell.shape[0]/2)
    #angle = lib.determine_angle([self.center_x, self.center_y], self.single_cell)
    #self.rotate_cell(angle)

  def init_figure(self):
    plt.close()
    self.single_cell_axes = plt.axes((0.05, 0.2, 0.4, 0.75))
    self.cross_section_axes = plt.axes((0.55, 0.2, 0.4, 0.75))  
    self.single_cell_axes.set_title('center cell')
    self.update_figure()
    self.adjust_cell()    
    ax = plt.axes((0.06, 0.25, 0.38, 0.65))
    ax.scatter(0, 0)
    ax.axvline(x=0, linestyle='dashed')
    ax.axhline(y=0, linestyle='dashed')
    ax.patch.set_alpha(0.5)
    plt.axis('off')
    self.cell_plot = self.single_cell_axes.imshow(self.single_cell)
    self.cid_1 = self.cell_plot.figure.canvas.mpl_connect('key_press_event', self.key_event)
    plt.draw()
    plt.show()

  def rotate_cell(self, angle):
    pad_size = int(self.magnification) * 3
    mean = np.mean(self.single_cell)
    cell = np.pad(self.single_cell, pad_size, 'mean')
    width = pad_size
    height = pad_size
    center_y = self.center_y + pad_size
    center_x = self.center_x + pad_size
    cell = cell[center_y-height:center_y+height, center_x-width:center_x+width]
    
    self.single_cell = ndimage.rotate(cell, angle)
    ind = np.where(self.single_cell == 0)
    self.single_cell[ind] = mean
    self.center_x = int(self.single_cell.shape[1]/2)
    self.center_y = int(self.single_cell.shape[0]/2)
    center_y = self.center_y 
    center_x = self.center_x
    self.single_cell = self.single_cell[center_y-height:center_y+height, center_x-width:center_x+width]
    self.center_x = int(self.single_cell.shape[1]/2)
    self.center_y = int(self.single_cell.shape[0]/2)

  def adjust_cell(self):
    for key_sav in self.key_save:
      magnitude = (self.magnitude * 5) + 1
      if (key_sav == 'left'):
        self.center_x += magnitude
      elif (key_sav == 'right'):
        self.center_x += -magnitude
      elif (key_sav == 'up'):
        self.center_y += -magnitude
      elif (key_sav == 'down'):
        self.center_y += magnitude
      elif (key_sav == 'a'):
        self.rotate_cell(-magnitude*3)
        self.single_cell_axes.imshow(self.single_cell)
      elif (key_sav == 'd'):
        self.rotate_cell(magnitude*3)
        self.single_cell_axes.imshow(self.single_cell)
      elif (key_sav == 'shift'):
        self.magnitude = 1 - self.magnitude
      self.update_figure()
    self.key_save = []
    

  def key_event(self, event):
    magnitude = (self.magnitude * 5) + 1
    self.key_save.append(event.key)
    if (event.key == 'enter'): 
      self.export()
      return
    elif (event.key == 'left'):
      self.center_x += magnitude
    elif (event.key == 'right'):
      self.center_x += -magnitude
    elif (event.key == 'up'):
      self.center_y += -magnitude
    elif (event.key == 'down'):
      self.center_y += magnitude
    elif (event.key == 'a'):
      self.rotate_cell(-magnitude*3)
      self.single_cell_axes.imshow(self.single_cell)
    elif (event.key == 'd'):
      self.rotate_cell(magnitude*3)
      self.single_cell_axes.imshow(self.single_cell)
    elif (event.key == 'shift'):
      self.magnitude = 1 - self.magnitude
    elif (event.key == 'q'):
      self.skip_cell()
    self.update_figure()

  def skip_cell(self):
    exit()
     
  def export(self):
    self.cell_plot.figure.canvas.mpl_disconnect(self.cid_1)
    plt.close()
    sample_name = fh.get_sample_name(self.file_name)
    replicate_name = fh.get_replicate_name(self.file_name)
    relative_path_file_name = os.path.join('single_cell_images', sample_name, replicate_name+'_cell_'+str(self.cell_number)+'.csv')
    relative_path = os.path.join('single_cell_images', sample_name)
    if True != os.path.exists(relative_path):
      os.makedirs(relative_path)
    height = self.magnification*2
    width = self.magnification
    center_y = self.center_y
    center_x = self.center_x
    cropped_cell = self.single_cell[center_y-height:center_y+height,center_x-width:center_x+width]
    np.savetxt(relative_path_file_name, cropped_cell, delimiter=',', fmt='%5.0u')
    print(relative_path_file_name)
  
  def get_key_save(self):
    return self.key_save
